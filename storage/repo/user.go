package repo

import (
	pb "customer_service/genproto/customer"
)

type CustomerStorageI interface {
	Create(*pb.CustomerRequest) (*pb.CustomerResponse, error)
	GetCustomerInfo(*pb.CustomerID) (*pb.CustomerInfo, error)
	UpdateCustomer(*pb.CustomerUp) (*pb.CustomerResponse, error)
	DeleteCustomer(*pb.CustomerID) (*pb.Empty, error)

	CheckFiedld(*pb.CheckFieldReq) (*pb.CheckFieldResp, error)
}
